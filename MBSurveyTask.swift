//http://care-dev-893213040.us-west-2.elb.amazonaws.com/help#SurveyTask

import RealmSwift

class MBSurveyTask: MBObject {

    var rules = List<MBSurveyResultStepNavigationRule>()
    var steps = List<MBSurveyStep>()
    dynamic var surveyTaskID = Int()
    dynamic var surveyTemplateID = Int()
    dynamic var taskOrderNumber = Int()

	convenience init(JSON json: Dictionary<String,AnyObject>?) {
		self.init(JSON: json, strFetchedBy:"")
	}

	convenience required init(JSON json: Dictionary<String,AnyObject>?, callingMethod: String) {
		self.init(JSON: json, strFetchedBy: callingMethod)

		if json == nil {
			return
		}
		if !MBRealm.isJSONValueNil( json, key: "rules" ) && json?["rules"] as? [Dictionary<String,AnyObject>] != nil, let arrDic = json?["rules"] as? [Dictionary<String,AnyObject>] {
				for dic in arrDic {
					rules.append( MBSurveyResultStepNavigationRule(JSON: dic, callingMethod: self.strFetchedBy ) )
				}
			}
		if !MBRealm.isJSONValueNil( json, key: "steps" ) && json?["steps"] as? [Dictionary<String,AnyObject>] != nil, let arrDic = json?["steps"] as? [Dictionary<String,AnyObject>] {
				for dic in arrDic {
					steps.append( MBSurveyStep(JSON: dic, callingMethod: self.strFetchedBy ) )
				}
			}
		if !MBRealm.isJSONValueNil( json, key: "surveyTaskID" ), let val = json?["surveyTaskID"] as? Int {
				self.surveyTaskID = val
		}
		if !MBRealm.isJSONValueNil( json, key: "surveyTemplateID" ), let val = json?["surveyTemplateID"] as? Int {
				self.surveyTemplateID = val
		}
		if !MBRealm.isJSONValueNil( json, key: "taskOrderNumber" ), let val = json?["taskOrderNumber"] as? Int {
				self.taskOrderNumber = val
		}
		self.updatePrimaryKey()
	}

	override func toDictionary() -> [String:AnyObject] {
		var dictionary = super.toDictionary()
		var rulesCollectionDictionary : [[String:AnyObject]] = [[String:AnyObject]]()
		for anyObject in self.rules {
		   rulesCollectionDictionary.append(anyObject.toDictionary())
		}
		dictionary["rules"] = rulesCollectionDictionary as AnyObject?
		var stepsCollectionDictionary : [[String:AnyObject]] = [[String:AnyObject]]()
		for anyObject in self.steps {
		   stepsCollectionDictionary.append(anyObject.toDictionary())
		}
		dictionary["steps"] = stepsCollectionDictionary as AnyObject?
		dictionary["surveyTaskID"] = self.surveyTaskID as AnyObject?
		dictionary["surveyTemplateID"] = self.surveyTemplateID as AnyObject?
		dictionary["taskOrderNumber"] = self.taskOrderNumber as AnyObject?
		dictionary[MBObject.strClassNameKey] = "MBSurveyTask" as AnyObject?
		return dictionary
	}

	override var description: String {
		return MBModelHelper.jsonFromMBObject(self)
	}

	override var hashValue: Int {
		return self.description.hash
	}
}

func ==(lhs: MBSurveyTask, rhs: MBSurveyTask) -> Bool {
	return lhs.hashValue == rhs.hashValue
}
