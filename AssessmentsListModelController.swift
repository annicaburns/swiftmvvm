//
//  AssessmentsListModelController.swift
//  CareManager
//
//  Created by Annica Burns on 7/3/16.
//  Copyright © 2016 Health Catalyst. All rights reserved.
//

import Foundation
import ResearchKit

class AssessmentsListModelController {
    
    let localCache: LocalObjectCache
    let patient: MBPatient
    
    private(set) var staticData: AssessmentStaticData?
    private(set) var surveyResults = [MBSurveyResult]()
    private(set) var filteredSurveyResults = [ExtendedSurveyResult]()
    
    // MARK: - Lifecycle
    
    init(localCache:LocalObjectCache, patient:MBPatient) {
        self.localCache = localCache
        self.patient = patient
    }
    
    // MARK: - Public Functions
    
    func applyFilters(_ filters: [Filter], completion: AssessmentsListFilterCompletion) {
        if let staticData = self.staticData {
            // Map the survey results into a collection of ExtendedSurveyResults to calculate the lockedStatus
            let extendedSurveyResults = self.surveyResults.map { ExtendedSurveyResult(surveyResult: $0, surveyEditWindowInMinutes: staticData.editWindowInMinutes) }
            
            // Filter the results
            if filters.isEmpty {
                self.filteredSurveyResults = extendedSurveyResults
            } else {
                self.filteredSurveyResults = extendedSurveyResults.filter { self.includeInFiltered(filters: filters, extendedSurveyResult: $0) }
            }
            
            // Sort the results
            self.filteredSurveyResults.sort { (leftExtendedResult, rightExtendedResult) -> Bool in
                if leftExtendedResult.surveyResult.isComplete != rightExtendedResult.surveyResult.isComplete && !leftExtendedResult.surveyResult.isComplete {
                    return true
                }
                if !leftExtendedResult.surveyResult.isComplete && !rightExtendedResult.surveyResult.isComplete {
                    return leftExtendedResult.surveyResult.startDate.compare(rightExtendedResult.surveyResult.startDate) == .orderedDescending
                }
                if leftExtendedResult.isLocked != rightExtendedResult.isLocked && !leftExtendedResult.isLocked {
                    return true
                }
                if leftExtendedResult.isLocked && rightExtendedResult.isLocked {
                    return leftExtendedResult.surveyResult.completeDate.compare(rightExtendedResult.surveyResult.completeDate) == .orderedDescending
                }
                
                return false
            }
        }

        completion()
    }
    
    private func includeInFiltered(filters: [Filter], extendedSurveyResult: ExtendedSurveyResult?) -> Bool {
        guard let extendedSurveyResult = extendedSurveyResult else { return false }
        guard let survey = extendedSurveyResult.surveyResult.surveyTemplate?.survey else { return false }
        
        var assessmentTypes = false
        var assessmentMethods = false
        var assessmentCategories = false
        var assessmentStatus = false
        
        for filter in filters {

            if filter.name == "filters.assessments.type".localized(), let surveyTypes = self.staticData?.surveyTypes {
                
                // Assessment Types
                if filter.selectedOptions.count == 0 {
                    assessmentTypes = true
                } else {
                    for surveyType in surveyTypes {
                        assessmentTypes = assessmentTypes || (filter.selectedOptions.contains(surveyType.surveyTypeName) ? survey.surveyTypeID == surveyType.surveyTypeID : false)
                    }
                }
            }
            
            if filter.name == "filters.assessments.method".localized(), let surveyMethods = self.staticData?.surveyMethods {
                
                // Assessment Methods
                if filter.selectedOptions.count == 0 {
                    assessmentMethods = true
                } else {
                    for surveyMethod in surveyMethods {
                        assessmentMethods = assessmentMethods || (filter.selectedOptions.contains(surveyMethod.surveyMethodName) ? survey.surveyMethodID == surveyMethod.surveyMethodID : false)
                    }
                }
            }

            if filter.name == "filters.assessments.category".localized(), let surveyCategories = self.staticData?.surveyCategories {
                
                // Assessment Categories
                if filter.selectedOptions.count == 0 {
                    assessmentCategories = true
                } else {
                    for surveyCategory in surveyCategories {
                        assessmentCategories = assessmentCategories || (filter.selectedOptions.contains(surveyCategory.surveyCategoryName) ? survey.surveyCategoryID == surveyCategory.surveyCategoryID : false)
                    }
                }
            }
            
            if filter.name == "filters.assessments.status".localized() {
                
                // Assessment Status
                if filter.selectedOptions.count == 0 {
                    assessmentStatus = true
                } else {
                    assessmentStatus = assessmentStatus || (!extendedSurveyResult.surveyResult.isComplete && filter.selectedOptions.contains(AssessmentSectionType.inProgress.sectionTitle))
                    assessmentStatus = assessmentStatus || (extendedSurveyResult.surveyResult.isComplete && !extendedSurveyResult.isLocked && filter.selectedOptions.contains(AssessmentSectionType.completedCanEdit.sectionTitle))
                    assessmentStatus = assessmentStatus || (extendedSurveyResult.surveyResult.isComplete && extendedSurveyResult.isLocked && filter.selectedOptions.contains(AssessmentSectionType.completed.sectionTitle))
                }
            }
        }
        
        return assessmentTypes && assessmentMethods && assessmentCategories && assessmentStatus
    }

    // Download any assessments that have been started or completed for this patient. Use the caching version of this call so that we can provide any cached version of the list when we are offline
    func fetchSurveyResults(_ completion: @escaping AssessmentsListCompletion) {
        
        let _ = AssessmentStaticData(localCache:self.localCache, callback: { (staticData: AssessmentStaticData?, staticDataError: Error?) -> Void in
            guard let assessmentStaticData = staticData else {
                completion(staticDataError)
                return
            }
            
            self.staticData = assessmentStaticData
            self.localCache.api?.GetFacilityFacilityIDPatientPatientIDSurveyresult(false, facilityID: self.localCache.facilityID, patientID: self.patient.patientID, limitToCompleted: "false", callback: { (surveyResults: [MBSurveyResult]?, isRefreshing, response, error) in
                if (error as? NSError)?.code == NSURLErrorNotConnectedToInternet {
                    // If we get a network reachability error on the second callback, leave the cached data in place.
                    completion(error)
                    return
                }
                // Load the returned data on both callbacks (so that have the cached data to display), but don't call the handler (to reload the table) until we know if we are finished.
                if let items = surveyResults {
                    self.surveyResults = items
                }
                if isRefreshing { return }
                completion(error)
            })
        })

    }
        
    func fetchAssessmentController(_ surveyResult: MBSurveyResult, completion: @escaping AssessmentsListFetchControllerCompletion) {
        AssessmentsHelper.prepareAssessmentEditControllerFromResult(surveyResult, localCache: self.localCache, completion: completion)
    }
    
    // Download the dataset needed to display a review of completed, locked Survey Results
    func fetchSurveyReviewData(_ surveyResult: MBSurveyResult, completion: @escaping AssessmentsListFetchReviewDataCompletion) {

        self.localCache.api?.GetFacilityFacilityIDPatientPatientIDSurveyresultSurveyResultIDSummary(false, facilityID: self.localCache.facilityID, patientID: self.patient.patientID, surveyResultID: surveyResult.surveyResultID, callback: { (summary, isRefreshing, response, error) in
            
            if let error = error {
                completion(nil, error)
                return
            }
            if isRefreshing == true { return }
            guard let resultSummary = summary else { return }
            let storyboard = UIStoryboard(name: "AssessmentReview", bundle: Bundle.main)
            let assessmentReviewController = storyboard.instantiateViewController(withIdentifier: "AssessmentReviewViewController") as! AssessmentReviewViewController
            assessmentReviewController.staticData = self.staticData
            assessmentReviewController.surveyResultScore = surveyResult.surveyResultScore
            let tempViewControllers: [QuestionReviewViewController] =  resultSummary.questionList.enumerated().map { (index, _) in
                let storyboard = UIStoryboard(name: "AssessmentReview", bundle: Bundle.main)
                let controller = storyboard.instantiateViewController(withIdentifier: "QuestionReviewViewController") as! QuestionReviewViewController
                controller.question = resultSummary.questionList[index]
                assessmentReviewController.addChildQuestionViewController(controller)
                return controller
            }
            assessmentReviewController.questionViewControllers = tempViewControllers
            assessmentReviewController.survey = resultSummary.survey
            completion(assessmentReviewController, nil)
            
        })
    }
    
}
