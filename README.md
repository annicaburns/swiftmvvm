# README #

### What is this repository for? ###

This repository contains a few files to demonstrate how one might use a "model controller" class to separate data layer code (data model, business logic, networking code, etc.) from UI code for a UITableView-based list View Controller in a Swift application for iOS. This pattern deviates from the MVC design pattern common in Apple development to gain some of the advantages of the MVVM design pattern that is popular in .net/c# world, namely: code that is more easily re-used, less fragile, and more testable.

NOTE: These files have dependencies that are not included in this repository, and therefore will not compile.


### Files ###

- AssessmentsListViewController.swift: "View" layer - builds our UI.
- AssessmentsListModelController.swift: "View Model" layer - delivers data to our View.
- AssessmentStaticData.swift: "Model" layer - downloads and shares cached, shared "meta-data".
- MBSurvey.swift: "Model" layer - data model auto-generated from our web service manifest.
- MBSurveyTaskExtension.swift: "Model" layer - extends our auto-generated model with client-specific business logic.

### Screenshot ###

![screenshot](https://bitbucket.org/annicaburns/swiftmvvm/raw/master/AssessmentsList.png)