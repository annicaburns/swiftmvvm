//
//  AssessmentStaticData.swift
//  CareManager
//
//  Created by Annica Burns on 6/28/16.
//  Copyright © 2016 Health Catalyst. All rights reserved.
//

import Foundation

/// Lookup Lists and associated accessors for Assessments

class AssessmentStaticData {
    
    // Initilizing an instance of this class will attempt to fetch data from the server for all of our lookup lists and settings required to work with Assessments
        // If we have no network connectivity but we have cached data, we will return the cached data.
        // If we have no network connectiity AND no cached data, we will return the NSURLErrorNotConnectedToInternet error with no data.
        // If at any point we encounter an error OTHER than NSURLErrorNotConnectedToInternet, we will return that error with no data.
    let errorDomain = "AssessmentStaticData"

    var surveyTypes = [MBSurveyType]()
    var surveyCategories = [MBSurveyCategory]()
    var surveyMethods = [MBSurveyMethod]()
    // Edit Window for Completed Surveys (minutes after completion until they are locked). Zero is a legitimate value.
    var editWindowInMinutes:Int = -1

    
    // Initializer 
    
    init(localCache:LocalObjectCache, callback: @escaping (_ staticData:AssessmentStaticData?, _ error: Error?) -> Void) {
        
        // NOTE: This is a recursive function because each API calls will potentially call back 2 times. Once for sure with the currently cached value, and a second time if the call isRefreshing with the Web Service call Response.
        
        var dataPopulationIsFinal = false
        
        var cachedSurveyTypes: [MBSurveyType]?
        var cachedSurveyCategories: [MBSurveyCategory]?
        var cachedSurveyMethods: [MBSurveyMethod]?
        
        func evaluateResults(_ error: Error? = nil) {
            if !dataPopulationIsFinal {
                if let error = error  {
                    dataPopulationIsFinal = true
                    callback(nil, error)
                } else if !self.surveyTypes.isEmpty && !self.surveyCategories.isEmpty && !self.surveyMethods.isEmpty && self.editWindowInMinutes >= 0 {   // Check HERE for the presence of records or acceptable values in all of our properties. If all of our network calls have finished we will EITHER have legitimate values OR we will have received an error response
                    dataPopulationIsFinal = true
                    callback(self, nil)
                }
            }
            // Swallow any callbacks that return after we have determined dataPopulationIsFinal and called back to the client
        }
        
        
        // Fetch data from server. All network requests must be made in parallel (Asynchronously). Do NOT nest network calls.
        
        // SurveyTypes
        localCache.api?.GetFacilityFacilityIDSurveytype(true, facilityID: localCache.facilityID, callback: { (types: [MBSurveyType]?, isRefreshing, response, error) in
            guard let items = types, !items.isEmpty else {
                if isRefreshing == false {
                    if let cachedItems = cachedSurveyTypes, let errorMessage = error as? NSError, errorMessage.code == NSURLErrorNotConnectedToInternet {
                        self.surveyTypes = cachedItems
                        evaluateResults()
                    } else if let error = error {
                        // We are either missing a network connection and DO NOT have cached data - or we received an error other than NSURLErrorNotConnectedToInternet
                        evaluateResults(error)
                    } else {
                        // We did not receive an error, but for some reason, we have no cached data and received no items back from the server
                        let manualError = ApplicationError.manualError("assessments.type_plural.title".localized().capitalized, domain: self.errorDomain)
                        evaluateResults(manualError)
                    }
                }
                return
            }
            
            if isRefreshing == true {
                cachedSurveyTypes = items
                return
            }
            self.surveyTypes = items
            evaluateResults()
        })

        // SurveyCategories
        localCache.api?.GetFacilityFacilityIDSurveycategory(true, facilityID: localCache.facilityID, callback: { (categories: [MBSurveyCategory]?, isRefreshing, response, error) in
            guard let items = categories, !items.isEmpty else {
                if isRefreshing == false {
                    if let cachedItems = cachedSurveyCategories, let errorMessage = error as? NSError, errorMessage.code == NSURLErrorNotConnectedToInternet {
                        self.surveyCategories = cachedItems
                        evaluateResults()
                    } else if let error = error {
                        // We are either missing a network connection and DO NOT have cached data - or we received an error other than NSURLErrorNotConnectedToInternet
                        evaluateResults(error)
                    } else {
                        // We did not receive an error, but for some reason, we have no cached data and received no items back from the server
                        let manualError = ApplicationError.manualError("assessments.category_plural.title".localized().capitalized, domain: self.errorDomain)
                        evaluateResults(manualError)
                    }
                }
                return
            }
            
            if isRefreshing == true {
                cachedSurveyCategories = items
                return
            }
            self.surveyCategories = items
            evaluateResults()
        })

        // SurveyMethods
        localCache.api?.GetFacilityFacilityIDSurveymethod(true, facilityID: localCache.facilityID, callback: { (methods: [MBSurveyMethod]?, isRefreshing, response, error) in
            guard let items = methods, !items.isEmpty else {
                if isRefreshing == false {
                    if let cachedItems = cachedSurveyMethods, let errorMessage = error as? NSError, errorMessage.code == NSURLErrorNotConnectedToInternet {
                        self.surveyMethods = cachedItems
                        evaluateResults()
                    } else if let error = error {
                        // We are either missing a network connection and DO NOT have cached data - or we received an error other than NSURLErrorNotConnectedToInternet
                        evaluateResults(error)
                    } else {
                        // We did not receive an error, but for some reason, we have no cached data and received no items back from the server
                        let manualError = ApplicationError.manualError("assessments.method_plural.title".localized().capitalized, domain: self.errorDomain)
                        evaluateResults(manualError)
                    }
                }
                return
            }
            
            if isRefreshing == true {
                cachedSurveyMethods = items
                return
            }
            self.surveyMethods = items
            evaluateResults()
        })
        
        localCache.fetchApplicationSettings { (settings: MBApplicationSettings?, error: Error?) in
            // This LocalObjectCache function handles caching for us so we don't need to worry about it here. It also only returns one callback.
            if let applicationSettings = settings {
                self.editWindowInMinutes = applicationSettings.surveyEditWindowInMinutes
                evaluateResults()
                return
            }
            evaluateResults(error)
        }
        
    }
    
    // MARK: - Accessor Functions

    func surveyTypeName(fromID id:Int) -> String? {
        let filteredArray = self.surveyTypes.filter { $0.surveyTypeID == id }
        if let item = filteredArray.first {
            return item.surveyTypeName
        } else {
            return nil
        }
    }
    
    func surveyTypeID(fromName name: String) -> Int? {
        let filteredArray = self.surveyTypes.filter { $0.surveyTypeName == name }
        if let item = filteredArray.first {
            return item.surveyTypeID
        } else {
            return nil
        }
    }
    
    func surveyCategoryName(fromID id: Int) -> String? {
        let filteredArray = self.surveyCategories.filter { $0.surveyCategoryID == id }
        if let item = filteredArray.first {
            return item.surveyCategoryName
        } else {
            return nil
        }
    }
    
    func surveyCategoryID(fromName name: String) -> Int? {
        let filteredArray = self.surveyCategories.filter { $0.surveyCategoryName == name }
        if let item = filteredArray.first {
            return item.surveyCategoryID
        } else {
            return nil
        }
    }
    
    func surveyMethodName(fromID id: Int) -> String? {
        let filteredArray = self.surveyMethods.filter { $0.surveyMethodID == id }
        if let item = filteredArray.first {
            return item.surveyMethodName
        } else {
            return nil
        }
    }
    
    func surveyMethodID(fromName name: String) -> Int? {
        let filteredArray = self.surveyMethods.filter { $0.surveyMethodName == name }
        if let item = filteredArray.first {
            return item.surveyMethodID
        } else {
            return nil
        }
    }

}
