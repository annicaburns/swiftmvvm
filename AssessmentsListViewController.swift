//
//  AssessmentsListViewController.swift
//  CareManager
//
//  Created by Annica Burns on 6/28/16.
//  Copyright © 2016 Health Catalyst. All rights reserved.
//

import UIKit
import ResearchKit

private extension Selector {
    static let handleRefresh = #selector(AssessmentsListViewController.handleRefresh(_:))
}

enum AssessmentSectionType: Int {
    case inProgress
    case webAssessment
    case completedCanEdit
    case completed
    
    var sectionTitle: String {
        switch self {
        case .inProgress:
            return "assessments.in_progress".localized()
        case .webAssessment:
            return "assessments.web_assessment".localized()
        case .completedCanEdit:
            return "assessments.completed_may_edit".localized()
        case .completed:
            return "assessments.completed".localized()
        }
    }
    
    static let sectionTitleArray: [String] = {
        var rawValue = 0
        var titleArray = [String]()
        while let type = AssessmentSectionType(rawValue: rawValue) {
            titleArray.append(type.sectionTitle)
            rawValue += 1
        }
        return titleArray
    }()
}

class AssessmentsListViewController: PopableViewController, UITableViewDataSource, UITableViewDelegate, SurveyTemplatesControllerDelegate, FiltersDelegate {
    
    @IBOutlet weak var performAssessmentButtonContainerView: UIView!
    @IBOutlet weak var performAssessmentButtonLabel: UILabel!
    @IBOutlet weak var performAssessmentInstructionsLabel: UILabel!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyDataSetView: EmptyDataSetView!
    
    var patient: MBPatient? {
        didSet {
            guard let patient = patient else { return }
            self.modelController = AssessmentsListModelController(localCache: self.localCache, patient: patient)
        }
    }
    
    var filtersViewController: FiltersViewController?
    var taskViewControllerDelegate: ORKTaskViewControllerDelegate?
    var surveyIsInProgress = false
    
    var modelController: AssessmentsListModelController?
    
    var didBecomeActiveToken: NSObjectProtocol?
    
    var loadingData: Bool = false
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: .handleRefresh, for: .valueChanged)
        
        return refreshControl
    }()

    var sortedAssessments = [(sectionType: AssessmentSectionType, sectionData: [ExtendedSurveyResult])]()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Re-calculate the edit windows and resort assessments every time the view appears to keep data fresh. This should update our list if the user leaves the app on this screen and sends it into the background and then returns and also if the user view a survey or result but does not make any updates that will cause the list to be refreshed
        if self.loadingData == false {
            self.applyFilters()
        }
        
        self.didBecomeActiveToken = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: nil, using: { [weak self] notification in
            self?.applyFilters()
            })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let token = self.didBecomeActiveToken {
            NotificationCenter.default.removeObserver(token)
            self.didBecomeActiveToken = nil
        }
    }

    // MARK: - Filters
    
    func assessmentFilters() -> [Filter] {

        let existingFilters = self.filtersViewController?.filters ?? [Filter]()
        
        var filters = [Filter]()
        
        if let assessmentTypes = self.modelController?.staticData?.surveyTypes {
            let assessmentTypeNames = assessmentTypes.map { $0.surveyTypeName }
            let filterName = "filters.assessments.type".localized()
            let selectedTypes = (existingFilters.filter { $0.name == filterName }).first?.selectedOptions ?? [String]()
            let assessmentTypeFilter = Filter(name: filterName, options: assessmentTypeNames, selectedOptions: selectedTypes)
            filters.append(assessmentTypeFilter)
        }
        
        if let assessmentCategories = self.modelController?.staticData?.surveyCategories {
            let assessmentCategoryNames = assessmentCategories.map { $0.surveyCategoryName }
            let filterName = "filters.assessments.category".localized()
            let selectedTypes = (existingFilters.filter { $0.name == filterName }).first?.selectedOptions ?? [String]()
            let assessmentCategoryFilter = Filter(name: filterName, options: assessmentCategoryNames, selectedOptions: selectedTypes)
            filters.append(assessmentCategoryFilter)
        }
        
        if let assessmentMethods = self.modelController?.staticData?.surveyMethods {
            let assessmentMethodNames = assessmentMethods.map { $0.surveyMethodName }
            let filterName = "filters.assessments.method".localized()
            let selectedTypes = (existingFilters.filter { $0.name == filterName }).first?.selectedOptions ?? [String]()
            let assessmentMethodFilter = Filter(name: filterName, options: assessmentMethodNames, selectedOptions: selectedTypes)
            filters.append(assessmentMethodFilter)
        }
        
        let filterName = "filters.assessments.status".localized()
        let selectedTypes = (existingFilters.filter { $0.name == filterName }).first?.selectedOptions ?? [String]()
        let statusFilter = Filter(name: filterName, options: AssessmentSectionType.sectionTitleArray, selectedOptions: selectedTypes)
        filters.append(statusFilter)

        return filters
    }
    
    func filtersUpdated(_ filters: [Filter]) {
        self.applyFilters()
    }
    
    func applyFilters() {
        if let modelController = self.modelController, let filtersViewController = self.filtersViewController {
            modelController.applyFilters(filtersViewController.filters, completion: {
                self.sortAssessments()
                self.tableView.isHidden = modelController.filteredSurveyResults.isEmpty
                self.tableView.reloadData()
            })
        }
    }
    
    // MARK: - Functions
    
    func layoutUI() {
        // View Controller and TableView
        self.title = "assessments.assessments_list.title".localized()
        self.tableView.tableFooterView = UIView()

        // Perform Assessment Button
        self.performAssessmentButtonContainerView.backgroundColor = StyleKit.green
        self.performAssessmentButtonContainerView.layer.cornerRadius = self.performAssessmentButtonContainerView.frame.height/10
        self.performAssessmentButtonLabel.text = "assessments.perform_assessment_button.label".localized()
        self.performAssessmentInstructionsLabel.text = "assessments.perform_assessment_button.instructions".localized()
        
        self.tableView.addSubview(self.refreshControl)

    }
    
    func loadData() {
        guard let modelController = self.modelController else { return }
        self.loadingData = true
        self.networkIndicatorView.showLoading(hostView: self.tableView)
        modelController.fetchSurveyResults() {
            error in
            self.filtersViewController?.filters = self.assessmentFilters()
            self.applyFilters()
            self.loadingData = false
            self.networkIndicatorView.processResponse(serverErrorResponse: error, controllerClass: self.className, objectClass: "common.network.object_class.assessments".localized(), hostView: self.tableView)
        }
    }
    
    func sortAssessments() {
        guard let modelController = self.modelController else { return }

        var webAssessmentArray = [ExtendedSurveyResult]()
        var inProgressArray = [ExtendedSurveyResult]()
        var completedCanEditArray = [ExtendedSurveyResult]()
        var completedArray = [ExtendedSurveyResult]()

        for surveyResult in modelController.filteredSurveyResults {
            if surveyResult.surveyResult.isComplete && surveyResult.isLocked {
                completedArray.append(surveyResult)
            } else if surveyResult.editOnlyOnWeb {
                webAssessmentArray.append(surveyResult)
            } else if !surveyResult.surveyResult.isComplete {
                inProgressArray.append(surveyResult)
            } else {
                completedCanEditArray.append(surveyResult)
            }
        }
        
        self.sortedAssessments = [(sectionType: .inProgress, sectionData: inProgressArray),
                                  (sectionType: .completedCanEdit, sectionData: completedCanEditArray),
                                  (sectionType: .webAssessment, sectionData: webAssessmentArray),
                                  (sectionType: .completed, sectionData: completedArray)]
        self.tableView.reloadData()
    }
    
    func presentAssessment(_ surveyResult: MBSurveyResult) {
        
        guard let modelController = self.modelController, let patient = self.patient else { return }
        
        if !self.surveyIsInProgress {
            self.networkIndicatorView.showLoading(isDimmingView: true)
            self.surveyIsInProgress = true
            modelController.fetchAssessmentController(surveyResult) { (taskViewController, stepIdentifierLookups, error) in
                self.networkIndicatorView.dismiss()
                guard let assessmentController = taskViewController, let stepIdentifierLookups = stepIdentifierLookups else {
                    self.surveyIsInProgress = false
                    if let error = error {
                        var controller = Alerts.errorAlertController("common.network.general_error_title".localized(), message: "common.network.general_error_text_short".localized())
                        if let definedError = error as? ApplicationError {
                            controller = Alerts.errorAlertController(definedError.title, message: definedError.description)
                        }
                        self.present(controller, animated: true, completion: nil)
                    }
                    return
                }
                self.taskViewControllerDelegate = CSTaskViewControllerDelegate(localCache: self.localCache, patient: patient, stepIdentifierLookups: stepIdentifierLookups, patientActionSurveyID: surveyResult.patientActionSurveyID, surveyResultLogDate: surveyResult.surveyResultLogDate, surveyResultID: surveyResult.surveyResultID, completeDate: surveyResult.completeDate, completionHandler: { (surveyWasSaved, saveError) in
                    self.taskViewControllerDelegate = nil
                    self.surveyIsInProgress = false
                    if surveyWasSaved { self.loadData() }
                    if saveError != nil {
                        let controller = Alerts.messageOnlyAlertController("common.network.cannot_save_assessment".localized())
                        self.present(controller, animated: true, completion: nil)
                    }
                })
                
                assessmentController.delegate = self.taskViewControllerDelegate
                self.present(assessmentController, animated: true, completion: nil)
            }
        }
    }
    
    func presentResults(_ surveyResult: MBSurveyResult) {
        
        guard let modelController = self.modelController else { return }
        self.networkIndicatorView.showLoading(isDimmingView: true)
        modelController.fetchSurveyReviewData(surveyResult) { (reviewViewController, error) in
            self.networkIndicatorView.dismiss()
            guard let reviewController = reviewViewController else {
                let controller = Alerts.messageOnlyAlertController("common.network.cannot_fetch_assessment_results.alert".localized())
                self.present(controller, animated: true, completion: nil)
                return
            }
            self.show(reviewController, sender: self)
        }
    }
    
    func presentWebEditAlert() {
        let controller = Alerts.errorAlertController("assessments.web_assessment_alert.title".localized(), message: "assessments.web_assessment_alert.message".localized())
        self.present(controller, animated: true, completion: nil)
    }
    
    // MARK: - User Actions
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        self.loadData()
    }
    
    @IBAction func performAssessmentButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AssessmentsControllerShowTemplatesController", sender: self)
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // MUST call super.prepareForSegue here because our base class is overriding it to pass along our LocalCache object
        super.prepare(for: segue, sender: sender)
        if let templatesViewController = segue.destination as? SurveyTemplatesViewController {
            templatesViewController.patient = self.patient
            templatesViewController.delegate = self
        }
        
        if let filtersViewController = segue.destination as? FiltersViewController {
            filtersViewController.delegate = self
            filtersViewController.filters = self.assessmentFilters()
            self.filtersViewController = filtersViewController
        }
    }

    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sortedAssessments.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.sortedAssessments[section].sectionData.count > 0 ? 20 : 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sortedAssessments[section].sectionType.sectionTitle
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = Bundle.main.loadNibNamed("SectionHeaderView", owner: self, options: nil)!.first as? SectionHeaderView
        sectionHeaderView?.titleLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        sectionHeaderView?.backgroundColor = StyleKit.backgroundMediumGray
        return sectionHeaderView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortedAssessments[section].sectionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyResultsCell", for: indexPath) as! SurveyResultsCell

        guard let modelController = self.modelController, let staticData = modelController.staticData else { return cell }
        cell.staticData = staticData
        cell.extendedSurveyResult = self.sortedAssessments[indexPath.section].sectionData[indexPath.row]
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        guard let modelController = self.modelController, let staticData = modelController.staticData else {
            return
        }
        let extendedSurveyResult = self.sortedAssessments[indexPath.section].sectionData[indexPath.row]
        // Re-calculate the extended Survey Result properties (isLocked, timeRemaining) because the data currently shown on screen could be stale
        let result = extendedSurveyResult.surveyResult
        let extendedSurvey = ExtendedSurveyResult(surveyResult: result, surveyEditWindowInMinutes: staticData.editWindowInMinutes)
        if extendedSurvey.isLocked {
            self.presentResults(result)
            return
        } else if extendedSurvey.editOnlyOnWeb {
            self.presentWebEditAlert()
            return
        }
    
        self.presentAssessment(extendedSurvey.surveyResult)
    }
    
    // MARK: - SurveyTemplatesControllerDelegate
    
    func AssessmentsReloadRequested() {
        self.loadData()
    }

}
