//
//  MBSurveyTaskExtension.swift
//  CareManager
//
//  Created by Annica Burns on 6/29/16.
//  Copyright © 2016 Health Catalyst. All rights reserved.
//

import Foundation
import ResearchKit

enum AssessmentTaskType {
    case orderedSurveyTask
    case navigableSurveyTask
    case invalid
    case unrecognized

}

extension MBSurveyTask {
    override func updatePrimaryKey() {
        mbPrimaryKey = "\(surveyTaskID)"
    }
    
    var type: AssessmentTaskType {
        get {
            if self.isRecognized {
                if !self.isValid { return .invalid }
                if !self.rules.isEmpty { return .navigableSurveyTask }
                return .orderedSurveyTask
            }
            return .unrecognized
        }
    }
    
    var isRecognized: Bool {
        get {
            // If we have even one unrecognized step or rule in the collection, mark the entire task as unrecognized
            var stepsAreRecognized = false
            let unrecognizedSteps = self.steps.filter { $0.isRecognized == false }
            stepsAreRecognized = unrecognizedSteps.isEmpty
            var rulesAreRecognized = false
            let unrecognizedRules = self.rules.filter { $0.isRecognized == false }
            rulesAreRecognized = unrecognizedRules.isEmpty
            if stepsAreRecognized && rulesAreRecognized { return true }
            
            return false
        }
    }
    
    var isValid: Bool {
        get {
            // The two types of assessments that we currently support (ordered and navigable) both require at least one step
            guard !self.steps.isEmpty else { return false }
            
            // All steps need to have a unique Step Identifier
            let steps: [MBSurveyStep] = Array(self.steps)
            let idArray = steps.flatMap { $0.transformedStepIdentifier }
            let idSet = Set(idArray)
            let duplicatesExist = idArray.count != idSet.count
            guard !duplicatesExist else { return false }

            // If we have even one invalid step or rule in the collection, invalidate the entire task
            var stepsAreValid = false
            var rulesAreValid = false
            let invalidSteps = self.steps.filter { $0.isValid == false }
            stepsAreValid = invalidSteps.isEmpty
            let invalidRules = self.rules.filter { $0.isValid == false }
            rulesAreValid = invalidRules.isEmpty
            if stepsAreValid && rulesAreValid { return true }
            return false
        }
    }
    
    func transformedFetchedSurveySteps(fetchedImagesDictionary: FetchedImagesDictionary) -> [ORKStep]? {
        guard self.type != .unrecognized, self.type != .invalid else { return nil }
        
        let steps: [MBSurveyStep] = Array(self.steps)
        // FlatMap will remove nil members
        let transformedSteps = steps.flatMap { $0.transformedFetchedSurveyStep(fetchedImagesDictionary: fetchedImagesDictionary) }
        
        guard !transformedSteps.isEmpty else { return nil }
        
        return transformedSteps
    }
    
    var stepIdentifierLookups: StepIdentifierLookup {
        get {
            var flattenedLookups = [String: Int]()
            let steps: [MBSurveyStep] = Array(self.steps)
            // FlatMap will remove nil members
            let lookups: [StepIdentifierLookup] = steps.flatMap { $0.stepIdentifierLookup }
            for lookup in lookups {
                for (key, value) in lookup {
                    flattenedLookups[key] = value
                }
            }
            return flattenedLookups
        }
    }
    
    func applyNavigationRulesToTask(_ task: ORKNavigableOrderedTask) -> ORKNavigableOrderedTask {
        for rule in self.rules {
            if let transformedRule = rule.transformedStepNavigationRule {
                task.setNavigationRule(transformedRule, forTriggerStepIdentifier: rule.transformedTriggerStepIdentifier)
            }
        }
        return task
    }
    
}
